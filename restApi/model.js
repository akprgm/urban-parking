var mongoose=require('mongoose');
var appSchemas=require('./schema');

module.exports={
    
    users: function usermodel(){
        return mongoose.model('users',appSchemas.users);
    },
    cars: function carmodel(){
        return mongoose.model('cars',appSchemas.cars);
       
    },
    space: function spacemodel(){
        return mongoose.model('space',appSchemas.space);
    },
    parking_temp: function parkingTempmodel(){
        return mongoose.model('parking_temp',appSchemas.parking_temp);
    },
    parking: function carmodel(){
        return mongoose.model('parking',appSchemas.parking);
    } 
        
}
 