var Express=require('express');
var router=Express.Router();

module.exports=function auth(passport){
    
    //sends successful login state back to client
	router.get('/users/success', function(req, res){
		res.send({state: 'success', user: req.user ? req.user : null});
	});

	//sends failure login state back to client
	router.get('/users/failure', function(req, res){
		res.send({state: 'failure', user: null, message: "Invalid username or password"});
	});

	//Log in 
	router.post('/users/login', passport.authenticate('login', {
		successRedirect: '/auth/users/success',
		failureRedirect: '/auth/users/failure'
	}));

	//sign up
	router.post('/users/signUp', passport.authenticate('signup', {
		successRedirect: '/auth/users/success',
		failureRedirect: '/auth/users/failure'
	}));

	//log out
	router.post('/users/signOut', function(req, res) {
		req.logout();
		res.end("logout successfully");
	});
    
    return router;
}