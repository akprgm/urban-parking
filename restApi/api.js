var fs=require('fs');
var Express=require('express');
var multiparty=require('multiparty');
var router=Express.Router();
var models=require('./model');
var user=models.users();
function isAuthenticated (req, res, next) {
	if (req.isAuthenticated()){
		return next();
	}

	// if the user is not authenticated then redirect him to the login page
	return res.end("User not Login");
};

router.use('/',isAuthenticated);

router.post('/users/update/',function(req,res){//route for update info of user
    console.log(req.user);
    var userEmail=req.user.user_email;
    if(userEmail===(req.user.user_email))
    {
        user.findOne({ 'user_email' :  userEmail }, function(err, data) {
            if (err){
                res.json({"success":false,"data":"Error in Updating profile"});
            }else if (data) {
                data.user_name=req.body.user_name;
                data.user_contact=req.body.user_contact;
                data.save(function err(err){
                    if(err)
                        res.json({"success":false,"data":"error in updating info"});
                    else
                        res.json({"success":true, "data":"successfully updated info"});
                });
            } 
        });    
     }else
        res.send("email not matched");     
});

router.post('/cars/addCar/',function(req,res){//route for adding car to user
    var userEmail=req.user.user_email;   
    var form = new multiparty.Form();
    form.parse(req, function(err, fields, files) {
        if(userEmail===(fields.user_email[0]))
        {
            var car=models.cars();
            var mycar=new car();
            mycar.car_user_email=fields.user_email[0];
            mycar.car_size=fields.car_size[0];
            mycar.car_color=fields.car_color[0];
            mycar.car_users=fields.car_users;
            mycar.car_rc_pic.data=fs.readFileSync(files.car_rc_pic[0].path);
            mycar.car_rc_pic.contentType = 'image/*';
            if(mycar.car_users)
                mycar.car_shared=true;
            mycar.save(function(err){    
                if (err){
                    res.json({"success":false,"data":"not able to insert new car"});  
                    throw err;  
                }
                res.json("successfully inserted new car",true);    
            });    
        }else
            res.json({"success":false, "data":"email not matched"});     
    });
});

router.get('/cars/getCars/',function(req,res){//route for getting car of user
    var userEmail=req.user.user_email;
    var cars=models.cars();
    cars.find({$or:[{'car_user_email':userEmail},{'car_users':userEmail}]},{'car_color':1,'car_size':1,'car_shared':1,'_id':1},function(err,cars){
        if(err)
            res.json({"success":false});
        else if(!cars.length)
            res.json({"success":false,"data":"Register few cars first!"});
        else
            res.json({"success":true,"data":cars});
    });
});

router.get('/cars/getDetails/',function(req,res){
    var car=models.cars();
    if(req.query.id){
        car.find({'_id':req.query.id},function(err,details){
            if(err){
                res.json({"success":false,"data":"error in getting detail of car"});
            }else if(!details){
                res.json({"success":false,"data":"Car with this Id dont exists"});
            }else{
                res.json({"success":true,"data":details});
            }
        });    
    }else{
        res.json({"success":false,"data":"failed in getting details of cars"})
    }
    
})

router.post('/cars/addUsers/',function(req,res){//route for adding users to a particular car
    var cars=mongoose.model('cars',appSchemas.cars);    
});

router.post('/space/registerSpace/',function(req,res){//route for registering space on app
    res.end(req.query.name);
});

router.get('/space/getSpace/',function(req,res){//route for getting free space
    
});
router.post('/space/update/',function(req,res){//route for updating space details
    
});
router.get('/parking/getHistory/',function(req,res){//route for getting parking history of user
    
});
router.post('/parking/register',function(req,res){//route for registering parking space
    
});

module.exports=router;


