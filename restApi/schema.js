var Mongoose=require('mongoose');
var Schema=Mongoose.Schema;
var cars=Mongoose.Schema;
var space=Mongoose.Schema;
var parking_temp=Mongoose.Schema;
var parking=Mongoose.Schema;
var users,cars,space,parking_temp,parking;
module.exports={
   users:new Schema({//user collection schema
        user_name:String,
        user_email:String,
        user_password:String,
        user_contact: Number,
        user_adhar:Number,
        user_profile_pic:String,
        adhar_pic: { data: Buffer, contentType: String },
    }),
    
    cars:new Schema({//car collection schema
        car_user_email:String,
        car_size:String,
        car_color: String,
        car_users:[String],
        car_shared:Boolean,
        car_rc_pic: { data: Buffer, contentType: String }
        
    }),
    
    space:new Schema({//space collection schema
        space_user_email:String,
        space_time_start:Date,
        space_time_end:Date,
        space_Date:Date,
        space_day:[String],
        space_pic: { data: Buffer, contentType: String }
    }),
    parking_temp:new Schema({//parking_temp collection schema
        user_email:String,
        space_id:String,
        car_id:String,
        parking_Date: Date,
        parking_timing:Date,
        adhar_no:Number,
        parking_amount:Number
    }),
    parking:new Schema({//parking collection schema
        user_email:String,
        space_id:String,
        car_id:String,
        parking_Date: Date,
        parking_timing:Date,
        adhar_no:Number,
        parking_amount:Number
    })
}