var port=8080;
var Express=require('express');
var bodyParser=require('body-parser');
var cookieParser=require('cookie-parser');
var session = require('express-session');
var passport = require('passport');
var logger=require('morgan');
var mongoose=require('mongoose');
var app=Express();1

var auth=require('./restapi/auth')(passport);
var api=require('./restapi/api');

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(session({
  secret: 'UrbanParking'
}));
app.use(cookieParser());
app.use(passport.initialize());
app.use(passport.session());
app.use('/auth/',auth);
app.use('/api/',api);
//// Initialize Passport
var initPassport = require('./restapi/passport-init');
initPassport(passport);


mongoose.connect('mongodb://localhost/parking');

app.listen(port,function(){
    console.log("Server started successfuly");
});
